// Copyright 2019 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package ginlog

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"time"
)

type stringer interface {
	String() string
}

// Logger returns a middleware that logs information about each request
// It tries to provide sensible defaults for CustomLogger
// These are:
//
// - Nothing is logged if the gin mode is set to test
//
// - If the gin mode is set to debug a colored non JSON printer is used (zerolog.NewConsoleWriter)
//
// - The zerolog.TimeFieldFormat is set to seconds since the unix epoch if gin mode is not set to debug (performance boost)
//
// If you need other settings just call CustomLogger
// These are the fields logged:
//   {
//     // if you want to change the level use CustomLogger
//     "level": "info",
//     // The timestamp when this was logged (after request)
//     // If you want to change this use CustomLogger and set zerolog.TimeFieldFormat yourself
//     "timestamp": "1566479585",
//     // The time it took to complete the request in zerolog.DurationFieldUnit (default: ms)
//     "latency": 0.14964,
//     // The client ip as returned by gin.Context
//     "client-ip": "93.184.216.34",
//     // The requesting HTTP method used
//     "method": "GET",
//     // The status of the response
//     "status": 200,
//     // The size in bytes of the returned body
//     "body-size": 854,
//     // The requested path with query
//     "path": "/",
//     // The errors passed to the gin.Context
//     "errs": [],
//     // Always served
//     "message": "served"
//   }
// Or just the following in debug mode:
//   <nil> INF served body-size=854 client-ip=93.184.216.34 errs=[] latency=0.14964 method=GET path=/ status=200 timestamp=2019-08-22T15:13:05+02:00
func Logger() gin.HandlerFunc {
	if gin.Mode() == gin.TestMode {
		return func(c *gin.Context) {
			c.Next()
		}
	}
	if gin.Mode() == gin.DebugMode {
		log.Logger.Output(zerolog.NewConsoleWriter())
	} else {
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	}
	return CustomLogger(log.Logger, zerolog.InfoLevel)
}

// CustomLogger returns a middleware that logs the following fields:
//   {
//     // the level you passed as lvl
//     "level": "info",
//     // The timestamp when this was logged (after request)
//     // If you want to change this set zerolog.TimeFieldFormat yourself or use Logger
//     "timestamp": "2019-08-22T15:13:05+02:00",
//     // The time it took to complete the request in zerolog.DurationFieldUnit (default: ms)
//     "latency": 0.14964,
//     // The client ip as returned by gin.Context
//     "client-ip": "93.184.216.34",
//     // The requesting HTTP method used
//     "method": "GET",
//     // The status of the response
//     "status": 200,
//     // The size in bytes of the returned body
//     "body-size": 854,
//     // The requested path with query
//     "path": "/",
//     // The errors passed to the gin.Context
//     "errs": [],
//     // Always served
//     "message": "served"
//   }
//
// If you want a set of sensible defaults use: Logger
// CustomLogger will only collect information if it isn't filtered by the level or sampler which produces almost zero overhead when it is disabled
// but even when enabled it produces little overhead thanks to the zero allocation logger it's build on.
func CustomLogger(log zerolog.Logger, lvl zerolog.Level, extraFields ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
		e := log.WithLevel(lvl)
		if !e.Enabled() {
			c.Next()
			return
		}

		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery
		defer func() {
			completedT := time.Now()
			if raw != "" {
				path = path + "?" + raw
			}
			e.
				Time("timestamp", completedT).
				TimeDiff("latency", completedT, start).
				Str("client-ip", c.ClientIP()).
				Str("method", c.Request.Method).
				Int("status", c.Writer.Status()).
				Int("body-size", c.Writer.Size()).
				Str("path", path).
				Strs("errs", c.Errors.Errors())
			for _, k := range extraFields {
				v, ok := c.Get(k)
				if ok {
					switch v.(type) {
					case string:
						e.Str(k, v.(string))
					case stringer:
						e.Str(k, v.(stringer).String())
					case int:
						e.Int(k, v.(int))
					case bool:
						e.Bool(k, v.(bool))
					default:
						e.Interface(k, v)
					}
				} else {
					e.RawJSON(k, []byte("null"))
				}
			}
			e.Msg("served")
		}()

		c.Next()
	}
}

// TODO add custom panic recovery which logs to the same logger
