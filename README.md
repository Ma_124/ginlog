# Ginlog
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/Ma_124/ginlog)](https://goreportcard.com/report/gitlab.com/Ma_124/ginlog)
[![GoDoc](https://godoc.org/gitlab.com/Ma_124/ginlog?status.svg)](https://godoc.org/gitlab.com/Ma_124/ginlog)
[![License MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://ma124.js.org/l/MIT/~/2019)

Ginlog provides a fast and easy to use logger for [gin][gin].

- `Logger()` provides a simple, no configuration required logger
- `CustomLogger(log, level)` gives you a bit more more control

These are the fields logged:
```js
{
  // the level you passed as lvl
  "level": "info",
  // The timestamp when this was logged
  "timestamp": "2019-08-22T15:13:05+02:00",
  // The time it took to complete the request
  "latency": 0.14964,
  // The client ip
  "client-ip": "93.184.216.34",
  // The requesting HTTP method used
  "method": "GET",
  // The status of the response
  "status": 200,
  // The size in bytes of the returned body
  "body-size": 854,
  // The requested path with query
  "path": "/",
  // The errors passed to the gin.Context
  "errs": [],
  // Always "served"
  "message": "served"
}
```

or in debug mode: ![<nil> INF served body-size=854 client-ip=93.184.216.34 errs=[] latency=0.14964 method=GET path=/ status=200 timestamp=2019-08-22T15:13:05+02:00](https://gitlab.com/Ma_124/pages-static-shared/raw/master/ginlog-screenshot1.png)

## Example

```go
package yourproject

import (
    "github.com/gin-gonic/gin"
    "gitlab.com/Ma_124/ginlog"
)

func main() {
    r := gin.New()
    // Just this single line
    r.Use(ginlog.Logger())
    r.Use(gin.Recovery())

    r.Run(":8080")
}
```

If you're on a Gin Middleware shopping tour check [Ginauth][ginauth] for among other things OAuth2 and JWT support out.

[gin]: https://github.com/gin-gonic/gin
[ginauth]: https://gitlab.com/Ma_124/ginauth#readme